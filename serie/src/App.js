import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import TodoForm from './components/todoForm';
import List from './components/list';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      todos: []
    }
  }

  onNewTodo(todo) {
    // console.log(todo);
    let newTodoList = this.state.todos;
    newTodoList.push(todo);
    this.setState({ todos: newTodoList });
  }

  toggleTodoState(todo, index){
    // console.log(todo, index);
    let _todo = todo;
    _todo.done = !todo.done;
    let newTodos = this.state.todos;
    newTodos[index] = _todo;
    this.setState({todos: newTodos})
  }

  render() {
    return (
      <div className="App">

        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Todo App</h2>
        </div>

        <div className="App-intro">
          <TodoForm onNewTodo={this.onNewTodo.bind(this)} />
          <List todos={this.state.todos}
            onTodoToggle={this.toggleTodoState.bind(this)}/>
        </div>

      </div>
    );
  }
}

export default App;
