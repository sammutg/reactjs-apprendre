import React, {Component} from 'react';

class List extends Component {
  render(){
    return(
      <div className="liste">
        Todos: [{this.props.todos.length}]
        {this.showTodos(this.props.todos)}
      </div>
    );
  }

  toggleState(todo, index) {
    //evt.preventDefault();
    //console.log('todo click', todo, index);
    this.props.onTodoToggle(todo, index);
  }

  showTodos(todos){
    return(
      todos.map((todo, index) => {
        return(
          <div className="todo"
            key={todo.createdAt}
            onClick={() => this.toggleState(todo, index)}>
            {todo.title} {todo.done ? 'v' : 'X'}
          </div>
        )
      })
    );
  }

}

export default List;
