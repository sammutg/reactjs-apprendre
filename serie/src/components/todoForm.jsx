import React, {Component} from 'react';

class TodoForm extends Component {

  onClick(evt) {
    evt.preventDefault();
    // console.log(this.todoField.value);
    const todoTxt = this.todoField.value;
    this.props.onNewTodo({
      title: todoTxt,
      done: false,
      createdAt: new Date()
    })
  }

  render(){
    return(
      <div className="todoForm">
        <input type="text" ref={(input) => this.todoField = input } />
        <button onClick={this.onClick.bind(this)}>Ajouter</button>
      </div>
    );
  }
}

export default TodoForm;
